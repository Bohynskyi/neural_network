import numpy as np  # helps with the math
import matplotlib.pyplot as plt  # to plot error during training
from PIL import Image
import glob

# Static file
PATH_IN = "Image/"


# create NeuralNetwork class
class NeuralNetwork:
    # intialize variables in class
    def __init__(self, inputs, outputs):
        self.inputs = inputs
        self.outputs = outputs
        # Rando
        np.random.seed(1)
        self.weights = np.random.rand(10000, 1)
        self.error_history = []
        self.epoch_list = []

    # activation function ==> S(x) = 1/1+e^(-x)
    def sigmoid(self, x, deriv=False):
        if deriv == True:
            return x * (1 - (x / 10000))
        return 1 / (1 + np.exp(-(x / 10000)))

    # data will flow through the neural network.
    def feed_forward(self):
        self.hidden = self.sigmoid(np.dot(self.inputs, self.weights))

    # going backwards through the network to update weights
    def backpropagation(self):
        self.error = self.outputs - self.hidden
        delta = self.error * self.sigmoid(self.hidden, deriv=True)
        self.weights += np.dot(self.inputs.T, delta)

    # train the neural net for 25,000 iterations
    def train(self, epochs=20000):
        for epoch in range(epochs):
            # flow forward and produce an output
            self.feed_forward()
            # go back though the network to make corrections based on the output
            self.backpropagation()
            # keep track of the error history over each epoch
            self.error_history.append(np.average(np.abs(self.error)))
            self.epoch_list.append(epoch)

    # function to predict output on new and unseen input data
    def predict(self, new_input):
        prediction = self.sigmoid(np.dot(new_input, self.weights))
        return prediction


if __name__ == '__main__':
    # input data
    inputs = np.zeros((6, 10000))
    image_list = glob.glob('{}*.jpg'.format(PATH_IN))  # Image list
    for i in range(len(image_list)):
        inputs[i, :] = (np.array(Image.open(image_list[i]).convert('L'))).reshape(1, 10000) * 0.003

    # output data
    outputs = np.array([[1], [0], [1], [0], [1], [1]])

    # create neural network
    NN = NeuralNetwork(inputs, outputs)

    # train neural network
    NN.train()

    # create two new examples to predict
    x = int(input('Number image: '))
    example = inputs[x, :]

    # print the predictions for both examples
    if NN.predict(example) <= 0.5:
        print("Smutny")
    else:
        print("Wesoły")

    # plot the error over the entire training duration
    plt.figure(figsize=(15, 5))
    plt.plot(NN.epoch_list, NN.error_history)
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.show()
