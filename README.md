# Detekcja uśmiechu #
>Póki co projekt jest podzielony na dwie cięści:
>1. Neural Network
>2. Program GUI

## Neural Network ##
Tu znajduje się program do uczenia maszynowego. Program jest oparty na
back propagation algorytm. Do intensyfikacji funkcji wykorzystuję sygmoide.
Sieć musi rozpoznać emocje, czyli czy obrazek jest uśmiechnięty (na wyjściu 1), czy smutny(o wyjściu 0).
Jako pliki uczące wykorzystałem 6 zdjęć.

#### Code ####
###### Import: ###### 
Dla poprawnego działanie projektu importujemy odpowiednie biblioteki:
```python
import numpy as np  # helps with the math
import matplotlib.pyplot as plt  # to plot error during training
from PIL import Image
import glob
```
###### Open Image: ###### 
Tą część kodu używamy do otwieranie plików (.jpg) służących do uczenia maszynowego,
gdzie `` PATH_IN = "Image/" ``. Otwieramy pliki komendą ``Image.open(image_list[i])`` oraz od razu konwertujemy plik do
skali szarości ``.convert('L')``. Komendę ``.reshape(1, 10000)`` używamy dla zmiany rozmiaru macierzy z obrazu do 10000x1.

```python
inputs = np.zeros((6, 10000))
image_list = glob.glob('{}*.jpg'.format(PATH_IN))  # Image list
for i in range(len(image_list)):
    inputs[i, :] = (np.array(Image.open(image_list[i]).convert('L'))).reshape(1, 10000) * 0.003
``` 

###### Class NeuralNetwork: ######
Obiekt tej klasy zawiera odpowiedne zmienne:
```python
def __init__(self, inputs, outputs):
    self.inputs = inputs
    self.outputs = outputs
    # Rando
    np.random.seed(1)
    self.weights = np.random.rand(10000, 1)
    self.error_history = []
    self.epoch_list = []
```
Gdzie ``inputs`` i ``outputs`` przykazujemy przy tworzeniu obiektu. Macierz ``weights`` która zawiera wartości neuronów 
oraz macierzy ``error_history`` i ``epoch_list`` wykorzystuję do zapisu przebiegu uczenia sieci.

###### Machine learning: ######
W tym projekcie do uczenia maszynowego wykorzystuje back propagation algorithm.
Oparty jest on na minimalizacji sumy kwadratów błędów (lub innej funkcji błędu) uczenia z wykorzystaniem 
optymalizacyjnej metody największego spadku:
```python
def backpropagation(self):
    self.error = self.outputs - self.hidden
    delta = self.error * self.sigmoid(self.hidden, deriv=True)
    self.weights += np.dot(self.inputs.T, delta)
```
Oraz dla identyfikacji wykorzystuję sigmoid:
```python
def sigmoid(self, x, deriv=False):
    if deriv == True:
        return x * (1 - (x / 10000))
    return 1 / (1 + np.exp(-(x / 10000)))
```

###### Sprawdzenie czy sieć działa poprawnie: ######
Do sprawdzania, czy sieć pracuje poprawnie, tymczasowo wykorzystuje zajęcza, z których korzystałem przy uczeniu. W przyszłości po dorobieniu GUI 
wykorzystanie takiego sposobu będzie niepotrzebnym.
```python
# create two new examples to predict
x = int(input('Number image: '))
example = inputs[x, :]

# print the predictions for both examples
if NN.predict(example) <= 0.5:
    print("Smutny")
else:
    print("Wesoły")
```
## Neural Network ##
Ta część projektu powinna składać się z pola do rysowania emocji oraz przyczynku
