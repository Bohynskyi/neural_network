from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt
import sys

class Ui_Dialog(object):

    def setupUi(self, Dialog):
        # Size windous
        weight = 600
        height = 400

        # Main Option
        Dialog.setObjectName("My project")
        Dialog.resize(height, weight)
        Dialog.setStyleSheet("background-color:#1f1f35;")

        # Label
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 450, 380, 50))
        self.label.setStyleSheet("color:#cbdeff;\n" "font: 26pt \"MS Shell Dlg 2\";")
        self.label.setText("")
        self.label.setObjectName("label")

        # Push Button
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(10, 510, 380, 50))
        self.pushButton.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.pushButton.setMouseTracking(False)
        self.pushButton.setStyleSheet("background-color:#00aaff;\n" "font: 18pt \"Times New Roman\";")
        self.pushButton.setObjectName("pushButton")

        self.canvas = Canvas(Dialog)

        w = QtWidgets.QWidget()
        l = QtWidgets.QVBoxLayout()
        w.setLayout(l)
        l.addWidget(self.canvas)

        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("My project", "My project"))
        self.pushButton.setText(_translate("Dialog", "Ok"))
        self.show()

    def mouseMoveEvent(self, e):
        print('hihi ')
        if self.last_x is None:  # First event.
            self.last_x = e.x()
            self.last_y = e.y()
            return  # Ignore the first time.

        painter = QtGui.QPainter(self.pixmap())
        p = painter.pen()
        p.setWidth(10)
        p.setColor(self.pen_color)
        painter.setPen(p)
        painter.drawLine(self.last_x, self.last_y, e.x(), e.y())
        painter.end()
        self.update()

        # Update the origin for next time.
        self.last_x = e.x()
        self.last_y = e.y()


    def mouseReleaseEvent(self, e):
        self.last_x = None
        self.last_y = None

class Canvas(QtWidgets.QLabel):

    def __init__(self, Dialog):
        super().__init__()
        label = QtWidgets.QLabel(Dialog)
        label.setGeometry(QtCore.QRect(10, 10, 380 ,390))

        pixmap = QtGui.QPixmap(380 ,390)
        self.setPixmap(pixmap)

        self.last_x = None
        self.last_y = None

        self.pen_color = QtGui.QColor('#ffffff')
        self.setStyleSheet("background-color: #141923")

        label.setPixmap(pixmap)
        #print(label.last_x,label.last_y)
        # pixmap = QtGui.QPixmap("image.jpg")


