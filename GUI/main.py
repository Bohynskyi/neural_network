from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from test import Ui_Dialog, Canvas


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()

    def bd():
        ui.label.setText('Hello, world!')

    ui.pushButton.clicked.connect(bd)

    #w = Example()
    sys.exit(app.exec_())